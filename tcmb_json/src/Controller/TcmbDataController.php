<?php


namespace Drupal\tcmb_json\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Cache\CacheableMetadata;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\Core\Database\Connection;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class TcmbDataController
 * @package Drupal\tcmb_json\Controller
 */
class TcmbDataController extends ControllerBase {

  /**
   * The database service.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

   /**
   * The aggregator.settings config object.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * Constructs.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   A database connection.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   */
  public function __construct(Connection $database, ConfigFactoryInterface $config_factory) {
    $this->database = $database;
    $this->config = $config_factory->get('tcmb.settings');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
      $container->get('tcmb.settings')
    );
  }


  /**
   * Get TCMB data
   * @return mixed
   */
  public function getTcmbData() {
    $records = $this->database->select('tcmb', 't')
      ->fields('t', [
        'updated'
      ])
      ->range(0, 1)
      ->orderBy('updated', 'DESC')
      ->execute()
      ->fetchAssoc();
    $tcmb_currency_codes = $this->config->get('tcmb_currency_codes', []);
    $updated = date('Y-m-d', strtotime($records['updated']));
    $records = $this->database->select('tcmb', 't')
      ->fields('t', [
        'currency_name',
        'currency_code',
        'updated',
        'buying',
        'selling'
      ])
      ->condition('currency_code', $tcmb_currency_codes, 'IN')
      ->condition('updated', $updated)
      ->groupBy('currency_code')
      ->groupBy('currency_name')
      ->groupBy('updated')
      ->groupBy('buying')
      ->groupBy('selling');
    return new JsonResponse($records->execute()->fetchAll(\PDO::FETCH_ASSOC));
  }

}
