<?php

/**
 * @RestResource(
 *   id = "tcmb_data_json_endpoint",
 *   label= @Translation("Tcmb data json enpoint"),
 *   uri_paths = {
 *    "canonical" = "/jsonapi/rest/tcmb"
 */
