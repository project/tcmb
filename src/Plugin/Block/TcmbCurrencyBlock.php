<?php

namespace Drupal\tcmb\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Database\Connection;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Render\Renderer;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'Tcmb Currency' block.
 *
 * @Block (
 *  id = "tcmb_block",
 *  admin_label = @Translation("Tcmb: currency"),
 *  category = @Translation("Tcmb")
 * )
 */
class TcmbCurrencyBlock extends BlockBase {

  /**
   * The database service.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The aggregator.settings config object.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * The module extension list.
   *
   * @var \Drupal\Core\Extension\ModuleExtensionList
   */
  protected $moduleExtensionList;

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * {@inheritdoc}
   */
  public function __construct(Connection $database, ConfigFactoryInterface $config_factory, ModuleExtensionList $module_extension_list, Renderer $renderer) {
    $this->database = $database;
    $this->config = $config_factory->get('tcmb.settings');
    $this->moduleExtensionList = $module_extension_list;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
      $container->get('tcmb.settings'),
      $container->get('extension.list.module'),
      $container->get('renderer')
    );
  }


  /**
   * {@inheritdoc}
   */
  public function build() {
    $records = $this->database->select('tcmb', 't')
      ->fields('t', [
        'updated',
      ])
      ->range(0, 1)
      ->orderBy('updated', 'DESC')
      ->execute()
      ->fetchAssoc();
    $updated = date('Y-m-d', strtotime($records['updated']));
    $tcmb_currency_codes = $this->config->get('tcmb_currency_codes', []);
    $records = $this->database->select('tcmb', 't')
      ->fields('t', [
        'currency_name',
        'currency_code',
        'updated',
        'buying',
        'selling',
      ])
      ->condition('currency_code', $tcmb_currency_codes, 'IN')
      ->condition('updated', $updated)
      ->groupBy('currency_code')
      ->groupBy('currency_name')
      ->groupBy('updated')
      ->groupBy('buying')
      ->groupBy('selling');
    $results = $records->execute()->fetchAll(\PDO::FETCH_ASSOC);
    $header = [t('Currency'), t('Buying'), t('Selling')];
    $rows = [];
    $build = [
      '#type' => 'table',
      '#header' => $header,
    ];
    foreach ($results as $result) {
      $title = $result['currency_name'];
      $image = $this->moduleExtensionList->getPath('tcmb') . '/png/' . strtolower($result['currency_code']) . '.png';
      $id = $result['currency_name'];
      $build[$id]['image'] = [
        '#theme' => 'image',
        '#uri' => $image,
        '#alt' => $title,
        '#title' => $title,
        '#suffix' => " " . $result['currency_code'],
      ];
      $build[$id]['buy'] = [
        '#markup' => $result['buying'],
      ];
      $build[$id]['sell'] = [
        '#markup' => $result['selling'],
      ];
    }
    return [
      '#markup' => $this->renderer->render($build),
      '#suffix' => ($this->config->get('tcmb_show_currency_block_footer')) ? t('Exchange rates are updated once a day on 15:30 on weekdays.') : '',
      '#cache' => [
        'max-age' => 50000,
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $records = $this->database->select('tcmb', 't')
      ->fields('t', [
        'updated',
      ])
      ->range(0, 1)
      ->orderBy('updated', 'DESC')
      ->execute()
      ->fetchAssoc();
    $updated = date('Y-m-d', strtotime($records['updated']));
    $tcmb_currency_codes = $this->config->get('tcmb_currency_codes', []);
    $records = $this->database->select('tcmb', 't')
      ->fields('t', [
        'currency_name',
        'currency_code',
        'updated',
        'buying',
        'selling',
      ])
      ->condition('currency_code', $tcmb_currency_codes, 'IN')
      ->condition('updated', $updated)
      ->groupBy('currency_code')
      ->groupBy('currency_name')
      ->groupBy('updated')
      ->groupBy('buying')
      ->groupBy('selling');
    $results = $records->execute()->fetchAll(\PDO::FETCH_ASSOC);
    return [
      'label' => ($this->config->get('tcmb_show_currency_block_title')) ? t('TCMB rates for :updated', [':updated' => date('d-m-Y', strtotime($results[0]['updated']))]) : '',
    ];
  }

}
