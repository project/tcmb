<?php

namespace Drupal\tcmb\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Configure Tcmb currency settings.
 */
class TcmbCurrencySettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'tcmb_currency_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, Request $request = NULL) {
    $config = $this->config('tcmb.settings');
    $form['tcmb_currency_codes'] = [
      '#type' => 'checkboxes',
      '#title' => t('Currency Codes'),
      '#default_value' => $config->get('tcmb_currency_codes') ?: [],
      '#options' => tcmb_currency_codes(),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (!array_filter($form_state->getValue('tcmb_currency_codes'))) {
      $form_state->setErrorByName('tcmb_currency_codes', $this->t('You need to set at least 1 currency.'));
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('tcmb.settings');
    $config->set('tcmb_currency_codes', $form_state->getValue('tcmb_currency_codes'));
    $config->save();
    return parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'tcmb.settings',
    ];
  }

}
