<?php

namespace Drupal\tcmb\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Configure Tcmb currency settings.
 */
class TcmbOtherSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'tcmb_other_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, Request $request = NULL) {
    $config = $this->config('tcmb.settings');
    $tcmb_currency_xml = new \DomDocument();
    $tcmb_currency_xml->load('http://www.tcmb.gov.tr/kurlar/today.xml');
    $form = [];
    $form['tcmb_currency_settings'] = [
      '#type' => 'fieldset',
      '#title' => t('Tcmb currency settings.'),
    ];
    $form['tcmb_currency_settings']['tcmb_show_currency_block_title'] = [
      '#type' => 'checkbox',
      '#title' => t('Show block title?'),
      '#default_value' => $config->get('tcmb_show_currency_block_title'),
    ];
    $form['tcmb_currency_settings']['tcmb_show_currency_block_footer'] = [
      '#type' => 'checkbox',
      '#title' => t('Show footer?'),
      '#default_value' => $config->get('tcmb_show_currency_block_footer'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('tcmb.settings');
    $config->set('tcmb_show_currency_block_title', $form_state->getValue('tcmb_show_currency_block_title'));
    $config->set('tcmb_show_currency_block_footer', $form_state->getValue('tcmb_show_currency_block_footer'));
    $config->save();
    return parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'tcmb.settings',
    ];
  }

}
