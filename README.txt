Tcmb module README file
-----------------------

What does Tcmb do?
---
Tcmb retrieves daily currency exchange codes from Turkish Republic Central Bank
(TCMB) and displays them nicely in a table. Tcmb module also retrieves gold
prices and displays them in another table.

How does it work?
---
Simple. Just enable the module at Admin > Extend, then go to
Admin > Configuration > Tcmb General Settings and enable some currency codes as
desired. Finally add the Tcmb block and move it to any region you want at
Admin > Structure > Blocks > Place Blocks.
